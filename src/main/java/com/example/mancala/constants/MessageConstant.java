package com.example.mancala.constants;

/**
 * Created by David
 **/
public interface MessageConstant {

    String TWO_PLAYER_REGISTRATION_LIMIT = "The total number of players that can be registered for the game is TWO";

    String BOTH_PLAYERS_CANNOT_BE_PLAYER_A = "Both players cannot be Player A";

    String BOTH_PLAYERS_CANNOT_BE_PLAYER_B = "Both players cannot be Player B";

    String PLAYER_REQUIRED = "The player is required";

    String PIT_NO_REQUIRED = "The pit number is required";

    String GAME_ID_REQUIRED = "Game ID is required";

    String YOU_CAN_ONLY_PICK_STONES_FROM_YOUR_DESIGNATED_PITS = "You can only pick stones from your designated pits";

    String PIT_HAS_NO_STONES = "The pit you selected has no stones";
}
