package com.example.mancala.constants;


public interface RouteConstant {

    String API = "/api";

    String GAME = API + "/game";

    String INITIALIZE = "/initialize";

    String SOW_STONES = "/sow-stones";

}
