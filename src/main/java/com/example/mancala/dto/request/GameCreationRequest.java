package com.example.mancala.dto.request;

import com.example.mancala.model.Player;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class GameCreationRequest {

    @Valid
    @NotEmpty(message = "Please provide a list of players")
    @NotNull(message = "Please provide a list of players")
    private List<Player> players;
}