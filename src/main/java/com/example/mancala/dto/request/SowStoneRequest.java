package com.example.mancala.dto.request;

import com.example.mancala.constants.MessageConstant;
import com.example.mancala.enums.PlayerCategory;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * Created by David
 **/
@Data
public class SowStoneRequest {

    @NotNull(message = MessageConstant.PLAYER_REQUIRED)
    private PlayerCategory playedBy;

    @NotNull(message = MessageConstant.PIT_NO_REQUIRED)
    private Integer pitIndex;

    @NotEmpty(message = MessageConstant.GAME_ID_REQUIRED)
    private String gameId;
}
