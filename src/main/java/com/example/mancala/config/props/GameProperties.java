package com.example.mancala.config.props;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by David
 **/
@Data
@Component
@ConfigurationProperties(prefix = "game.mancala")
public class GameProperties {

    private Integer numberOfPits;

    private Integer numberOfStonesPerPit;
}
