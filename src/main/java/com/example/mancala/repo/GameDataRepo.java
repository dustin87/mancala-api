package com.example.mancala.repo;

import com.example.mancala.model.GameData;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by David
 **/
public interface GameDataRepo extends CrudRepository<GameData, String> {
}
