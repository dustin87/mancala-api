package com.example.mancala.controller;

import com.example.mancala.constants.RouteConstant;
import com.example.mancala.dto.request.GameCreationRequest;
import com.example.mancala.dto.request.SowStoneRequest;
import com.example.mancala.service.GameService;
import com.fasterxml.jackson.core.JsonProcessingException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@Slf4j
@CrossOrigin
@RestController
@RequestMapping(RouteConstant.GAME)
@AllArgsConstructor
@Api(value = "Mancala game API. Set of endpoints for initializing and Sowing the Game")
public class GameController {

    private GameService gameService;

    @PostMapping
    @ApiOperation(value = "Endpoint for creating new Mancala game instance. It returns a Game object with unique " +
            "GameId used for sowing the game")
    public ResponseEntity<?> initializeGame(@RequestBody @Valid GameCreationRequest req)
            throws JsonProcessingException {

        return ResponseEntity.ok(gameService.initializeGame(req));
    }


    @PatchMapping(value = RouteConstant.SOW_STONES)
    @ApiOperation(value = "Endpoint for sowing the game. It keeps the history of the Game instance.")
    public ResponseEntity<?> sowStones(@RequestBody @Valid SowStoneRequest req) {

        return ResponseEntity.ok(gameService.sowStones(req));
    }


    @GetMapping(value = "/{id}")
    @ApiOperation(value = "Get game data")
    public ResponseEntity<?> getGameData(@PathVariable(value = "id") String gameId) {

        return ResponseEntity.ok(gameService.getGameData(gameId));

    }

}
