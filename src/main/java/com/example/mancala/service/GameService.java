package com.example.mancala.service;


import com.example.mancala.dto.request.GameCreationRequest;
import com.example.mancala.dto.request.SowStoneRequest;
import com.example.mancala.model.GameData;
import com.fasterxml.jackson.core.JsonProcessingException;

public interface GameService {
    GameData initializeGame(GameCreationRequest req) throws JsonProcessingException;

    GameData sowStones(SowStoneRequest request);

    GameData getGameData(String id);
}
