package com.example.mancala.service.impl;

import com.example.mancala.config.props.GameProperties;
import com.example.mancala.constants.MessageConstant;
import com.example.mancala.dto.request.GameCreationRequest;
import com.example.mancala.dto.request.SowStoneRequest;
import com.example.mancala.exception.ConflictException;
import com.example.mancala.exception.ForbiddenException;
import com.example.mancala.exception.NotFoundException;
import com.example.mancala.model.GameData;
import com.example.mancala.enums.PlayerCategory;
import com.example.mancala.exception.BadRequestException;
import com.example.mancala.model.Pit;
import com.example.mancala.repo.GameDataRepo;
import com.example.mancala.service.GameService;
import com.example.mancala.utils.GeneralUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
@Transactional
public class GameServiceImpl implements GameService {

    private GameDataRepo gameDataRepo;

    private GameProperties gameProperties;

    public static final int PLAYER_A_MANCALA_PIT_INDEX = 6;
    public static final int PLAYER_B_MANCALA_PIT_INDEX = 13;

    @Override
    public GameData initializeGame(GameCreationRequest req) throws JsonProcessingException {

        // validations
        if (req.getPlayers().size() != 2) {
            throw new BadRequestException(MessageConstant.TWO_PLAYER_REGISTRATION_LIMIT);
        }

        if (req.getPlayers().stream()
                .allMatch(p ->p.getCategory().equals(PlayerCategory.PLAYER_A))) {
            throw new BadRequestException(MessageConstant.BOTH_PLAYERS_CANNOT_BE_PLAYER_A);
        }

        if (req.getPlayers().stream()
                .allMatch(p -> p.getCategory().equals(PlayerCategory.PLAYER_B))) {
            throw new BadRequestException(MessageConstant.BOTH_PLAYERS_CANNOT_BE_PLAYER_B);
        }

        // create game
        GameData gameData = new GameData();
        gameData.setId(generateGameId());
        gameData.setPlayers(req.getPlayers());
        gameData.setNextPlayer(PlayerCategory.PLAYER_A);
        gameData.setCreatedAt(LocalDateTime.now());
        gameData.setUpdatedAt(LocalDateTime.now());
        gameData.setTtlSeconds(3600L);
        gameData.setPits(buildPits());

        return gameDataRepo.save(gameData);

    }

    @Override
    public GameData sowStones(SowStoneRequest request) {

        GameData gameData = getGameData(request.getGameId());

        // validations

        //1. check if the game is ended already
        if (gameData.getIsCompleted()) {
            throw new ConflictException("Its game over already!");
        }

        //2. check to ensure the Player is valid
        if (!request.getPlayedBy().equals(gameData.getNextPlayer())) {
            throw new ForbiddenException(String.format("%s should play next",
                    GeneralUtils.removeUnderscores(gameData.getNextPlayer().name())));
        }

        //3. check to ensure the player picked stones from the right pit
        if (request.getPlayedBy().equals(PlayerCategory.PLAYER_A) &&
                (request.getPitIndex() > 5 )) {
            throw new BadRequestException(MessageConstant.YOU_CAN_ONLY_PICK_STONES_FROM_YOUR_DESIGNATED_PITS);
        }

        if (request.getPlayedBy().equals(PlayerCategory.PLAYER_B) &&
                (request.getPitIndex() < 7 || request.getPitIndex() > 12 )) {
            throw new BadRequestException(MessageConstant.YOU_CAN_ONLY_PICK_STONES_FROM_YOUR_DESIGNATED_PITS);
        }


        // sow the stones into the corresponding pits
        List<Pit> pits = gameData.getPits();
        final Pit startingPit = pits.get(request.getPitIndex());

        if (startingPit.getStoneCount() < 1) {
            throw new BadRequestException(MessageConstant.PIT_HAS_NO_STONES);
        }

        int indexOfLastSowedPit = -1;

        int startFrom = 0;
        int endAt = startingPit.getStoneCount();

        pits.get(request.getPitIndex()).setStoneCount(0);       // set the starting-pit stone count to 0

        for (int i = startFrom; i < endAt; i++) {

            int pitIndex;

            if (indexOfLastSowedPit == -1)
                pitIndex = getNextPitIndex(startingPit.getIndex(), request.getPlayedBy());
            else
                pitIndex = getNextPitIndex(indexOfLastSowedPit, request.getPlayedBy());

            // update pits
            pits.get(pitIndex).setStoneCount(pits.get(pitIndex).getStoneCount() + 1);

            indexOfLastSowedPit = pitIndex;
        }


        // if the last stone lands in an own empty pit, the player captures his own stone and all stones in the
        // opposite pit (the other player’s pit) and puts them in his own mancala pit
        pits = updatePitsIfTheLastPitSowedIsAnOwnEmptyPit(request.getPlayedBy(), pits, indexOfLastSowedPit);




        // at this point, has the game ended?
        if (hasTheGameEnded(pits))
        {
            // collect the stones from the individual player's pit and put them in their respective Mancala's pit
            int remainingStonesForPlayerA = pits.stream()
                    .filter(p -> p.getIndex() < PLAYER_A_MANCALA_PIT_INDEX)
                    .mapToInt(Pit::getStoneCount)
                    .sum();

            int remainingStonesForPlayerB = pits.stream()
                    .filter(p -> p.getIndex() > PLAYER_A_MANCALA_PIT_INDEX && p.getIndex() < PLAYER_B_MANCALA_PIT_INDEX)
                    .mapToInt(Pit::getStoneCount)
                    .sum();


                                            // updating mancala pits
            pits.get(PLAYER_A_MANCALA_PIT_INDEX).setStoneCount(pits.get(PLAYER_A_MANCALA_PIT_INDEX).getStoneCount() + remainingStonesForPlayerA);
            pits.get(PLAYER_B_MANCALA_PIT_INDEX).setStoneCount(pits.get(PLAYER_B_MANCALA_PIT_INDEX).getStoneCount() + remainingStonesForPlayerB);

                                            // reset all other pits to 0
            pits.stream()
                    .filter(p -> p.getIndex() != PLAYER_A_MANCALA_PIT_INDEX && p.getIndex() != PLAYER_B_MANCALA_PIT_INDEX)
                    .forEach(p -> p.setStoneCount(0));



            // update game data
            gameData.setPits(pits);
            gameData.setLastUpdatedPitIndex(indexOfLastSowedPit);
            gameData.setWinner(getGameWinner(pits));
            gameData.setIsCompleted(true);
        }

        else {

            gameData.setPits(pits);
            gameData.setLastPlayer(gameData.getNextPlayer());
            gameData.setNextPlayer(gameData.getLastPlayer().equals(PlayerCategory.PLAYER_A)
                    ? PlayerCategory.PLAYER_B : PlayerCategory.PLAYER_A);

            if (request.getPlayedBy().equals(PlayerCategory.PLAYER_A) && indexOfLastSowedPit == PLAYER_A_MANCALA_PIT_INDEX) {
                gameData.setNextPlayer(PlayerCategory.PLAYER_A);
            }
            if (request.getPlayedBy().equals(PlayerCategory.PLAYER_B) && indexOfLastSowedPit == PLAYER_B_MANCALA_PIT_INDEX) {
                gameData.setNextPlayer(PlayerCategory.PLAYER_B);
            }

            gameData.setLastUpdatedPitIndex(indexOfLastSowedPit);
        }

        return gameDataRepo.save(gameData);
    }



    @Override
    public GameData getGameData(String id) {
        return gameDataRepo.findById(id).orElseThrow(
                () -> new NotFoundException("Game data not found"));
    }


    private UUID generateGameId() {
        return java.util.UUID.randomUUID();
    }

    public List<Pit> buildPits() {

        Integer noOfPits = gameProperties.getNumberOfPits();
        Integer noOfStonesPerPit = gameProperties.getNumberOfStonesPerPit();

        List<Pit> pits = new ArrayList<>();

        for (int i = 0; i < noOfPits; i++) {

            if (i == PLAYER_A_MANCALA_PIT_INDEX || i == PLAYER_B_MANCALA_PIT_INDEX)
            {
                pits.add(new Pit(i, 0));   // mancala pits should have 0 stones
            }
            else {
                pits.add(new Pit(i, noOfStonesPerPit));      // other pits should have the default no of stones
            }

        }

        return pits;
    }


    // if the last stone lands in an own empty pit, the player captures his own stone and all stones in the
    //opposite pit (the other player’s pit) and puts them in his own mancala pit
    public List<Pit> updatePitsIfTheLastPitSowedIsAnOwnEmptyPit(PlayerCategory player, List<Pit> pits,
                                                                int indexOfLastSowedPit) {
        Pit lastPitSowed = pits.get(indexOfLastSowedPit);

        if (player.equals(PlayerCategory.PLAYER_A)
                && lastPitSowed.getStoneCount() == 1
                    && lastPitSowed.getIndex() < 6 )
        {

            Pit oppositePit = pits.get(getOppositePitIndex(indexOfLastSowedPit));
            pits.get(PLAYER_A_MANCALA_PIT_INDEX).setStoneCount(pits.get(PLAYER_A_MANCALA_PIT_INDEX).getStoneCount() +
                    oppositePit.getStoneCount() + 1);

            pits.get(indexOfLastSowedPit).setStoneCount(0);
            pits.get(getOppositePitIndex(indexOfLastSowedPit)).setStoneCount(0);
        }

        else if (player.equals(PlayerCategory.PLAYER_B)
                    && lastPitSowed.getStoneCount() == 1
                        && lastPitSowed.getIndex() > 6
                            && lastPitSowed.getIndex() < 13){

            Pit oppositePit = pits.get(getOppositePitIndex(indexOfLastSowedPit));
            pits.get(PLAYER_B_MANCALA_PIT_INDEX).setStoneCount(pits.get(PLAYER_B_MANCALA_PIT_INDEX).getStoneCount() +
                    oppositePit.getStoneCount() + 1);

            pits.get(indexOfLastSowedPit).setStoneCount(0);
            pits.get(getOppositePitIndex(indexOfLastSowedPit)).setStoneCount(0);

        }

        return pits;
    }

    // If Player A or Player B has sowed all their stones, the game has ended
    public Boolean hasTheGameEnded(List<Pit> pits) {


        Boolean hasPlayerASowedAllStones = pits.stream()
                                        .filter(p -> p.getIndex() < PLAYER_A_MANCALA_PIT_INDEX)
                                        .noneMatch(p -> p.getStoneCount() > 0);

        Boolean hasPlayerBSowedAllStones = pits.stream()
                                        .filter(p -> p.getIndex() > PLAYER_A_MANCALA_PIT_INDEX &&
                                                p.getIndex() < PLAYER_B_MANCALA_PIT_INDEX)
                                        .noneMatch(p -> p.getStoneCount() > 0);


        return hasPlayerASowedAllStones || hasPlayerBSowedAllStones;
    }

    public int getOppositePitIndex(int pitIndex) {
        return (12 - pitIndex);
    }

    public PlayerCategory getGameWinner(List<Pit> pits) {

        // if stone counts from both Mancala pits are equal, then there is NO winner
        if (pits.get(PLAYER_A_MANCALA_PIT_INDEX).getStoneCount().equals(pits.get(PLAYER_B_MANCALA_PIT_INDEX).getStoneCount())) {
            return null;
        }

        else
            return pits.get(PLAYER_A_MANCALA_PIT_INDEX).getStoneCount() >
                        pits.get(PLAYER_B_MANCALA_PIT_INDEX).getStoneCount() ? PlayerCategory.PLAYER_A :
                                                                                    PlayerCategory.PLAYER_B;
    }

    private Integer getNextPitIndex(int currentPitIndex, PlayerCategory player) {

        int nextPitIndex = ++currentPitIndex;

        // .. player A shouldn't sow in pit 13
        if (player.equals(PlayerCategory.PLAYER_A) && nextPitIndex == PLAYER_B_MANCALA_PIT_INDEX) {
            ++nextPitIndex;
        }

        //.. player B shouldn't sow into pit 6
        if (player.equals(PlayerCategory.PLAYER_B) && nextPitIndex == PLAYER_A_MANCALA_PIT_INDEX) {
            ++nextPitIndex;
        }

        // recalculate the index
        return nextPitIndex > 13 ? (nextPitIndex - 14) : nextPitIndex;
    }

}
