package com.example.mancala.utils;


import com.google.common.base.Strings;

public class GeneralUtils {

    public static String removeUnderscores(String string) {
        if (Strings.isNullOrEmpty(string)) {
            return string;
        }

        return string.replaceAll("_", " ");
    }

}
