package com.example.mancala.model;

import com.example.mancala.constants.DateTimeEnum;
import com.example.mancala.enums.PlayerCategory;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.TimeToLive;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Data
@RedisHash(value = "GameData")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class GameData {

    @Id
    private UUID id;

    private List<Player> players;

    private List<Pit> pits;

    private PlayerCategory lastPlayer;

    private PlayerCategory nextPlayer;

    private Integer lastUpdatedPitIndex;

    private Boolean isCompleted = false;

    private PlayerCategory winner;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DateTimeEnum.DATE_TIME_PATTERN)
    private LocalDateTime createdAt;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DateTimeEnum.DATE_TIME_PATTERN)
    private LocalDateTime updatedAt;

    @TimeToLive
    private Long ttlSeconds;

}
