package com.example.mancala.model;

import com.example.mancala.enums.PlayerCategory;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * Created by David
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Player {

    @NotEmpty(message = "Player full name is required")
    private String fullName;

    @NotNull(message = "Player category is required")
    private PlayerCategory category;
}
