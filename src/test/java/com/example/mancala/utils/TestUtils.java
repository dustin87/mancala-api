package com.example.mancala.utils;

import com.example.mancala.model.Pit;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by David
 **/
public class TestUtils {

    public static List<Pit> generatePits() {
        int noOfPits = 14;
        int noOfStonesPerPit = 6;

        List<Pit> pits = new ArrayList<>();

        for (int i = 0; i < noOfPits; i++) {

            if (i == 6 || i == 13)
            {
                pits.add(new Pit(i, 0));   // mancala pits should have 0 stones
            }
            else {
                pits.add(new Pit(i, noOfStonesPerPit));
            }
        }

        return pits;
    }
}
