package com.example.mancala.test;

import com.example.mancala.config.props.GameProperties;
import com.example.mancala.constants.MessageConstant;
import com.example.mancala.dto.request.GameCreationRequest;
import com.example.mancala.dto.request.SowStoneRequest;
import com.example.mancala.enums.PlayerCategory;
import com.example.mancala.exception.BadRequestException;
import com.example.mancala.exception.ConflictException;
import com.example.mancala.exception.ForbiddenException;
import com.example.mancala.exception.NotFoundException;
import com.example.mancala.model.GameData;
import com.example.mancala.model.Pit;
import com.example.mancala.model.Player;
import com.example.mancala.repo.GameDataRepo;
import com.example.mancala.service.impl.GameServiceImpl;
import com.example.mancala.utils.TestUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by David
 **/
@RunWith(MockitoJUnitRunner.Silent.class)
public class GameServiceTest {

    @InjectMocks
    private GameServiceImpl gameService;

    @Mock
    private GameDataRepo gameDataRepo;

    @Mock
    private GameProperties gameProperties;

    @Captor
    private ArgumentCaptor<GameData> gameDataArgumentCaptor;

    GameData  gameData = new GameData();
    List<Player> players = new ArrayList<>();

    @Before
    public void init() {

        Player player = new Player("Tom brody", PlayerCategory.PLAYER_A);
        Player player2 = new Player("Steve Jobs", PlayerCategory.PLAYER_B);
        players.add(player);
        players.add(player2);


        gameData.setId(java.util.UUID.randomUUID());
        gameData.setPlayers(players);
        gameData.setPits(TestUtils.generatePits());
        gameData.setLastPlayer(null);
        gameData.setNextPlayer(PlayerCategory.PLAYER_A);
        gameData.setLastUpdatedPitIndex(null);
        gameData.setIsCompleted(false);
        gameData.setWinner(null);
        gameData.setCreatedAt(LocalDateTime.now());
        gameData.setUpdatedAt(LocalDateTime.now());

        System.out.println("game data: " + gameData.getId());


    }

    @Test
    public void testInitializeGame_throwsExceptionForPlayerSize() {

        // data
        GameCreationRequest req = new GameCreationRequest();
        Player player = new Player("Tom brody", PlayerCategory.PLAYER_A);
        req.setPlayers(Collections.singletonList(player));

        // test
        assertThrows(BadRequestException.class, ()->
                gameService.initializeGame(req), MessageConstant.TWO_PLAYER_REGISTRATION_LIMIT);

    }

    @Test
    public void testInitializeGame_throwsExceptionIfPlayersHaveTheSameCategory() {

        // data
        GameCreationRequest req = new GameCreationRequest();
        Player player = new Player("Tom brody", PlayerCategory.PLAYER_A);
        Player player2 = new Player("Steve Jobs", PlayerCategory.PLAYER_A);

        List<Player> players = new ArrayList<>();
        players.add(player);
        players.add(player2);

        req.setPlayers(players);

        // test
        assertThrows(BadRequestException.class, ()->
                gameService.initializeGame(req), MessageConstant.BOTH_PLAYERS_CANNOT_BE_PLAYER_A);

    }

    @Test
    public void testInitializeGame_successfully() throws JsonProcessingException {

        // data
        GameCreationRequest req = new GameCreationRequest();
        req.setPlayers(players);

        // when
        when(gameProperties.getNumberOfPits()).thenReturn(14);
        when(gameProperties.getNumberOfStonesPerPit()).thenReturn(6);


        // test
        gameService.initializeGame(req);

        // verify
        verify(gameDataRepo).save(any());
        verify(gameDataRepo).save(gameDataArgumentCaptor.capture());
        assertThat(gameDataArgumentCaptor.getValue()).isNotNull();
        assertThat(gameDataArgumentCaptor.getValue().getPlayers().size()).isEqualTo(2);
        assertThat(gameDataArgumentCaptor.getValue().getPits().size()).isEqualTo(14);
        assertThat(gameDataArgumentCaptor.getValue().getIsCompleted()).isFalse();
        assertThat(gameDataArgumentCaptor.getValue().getNextPlayer()).isEqualTo(PlayerCategory.PLAYER_A);
        assertThat(gameDataArgumentCaptor.getValue().getLastPlayer()).isNull();
        assertThat(gameDataArgumentCaptor.getValue().getWinner()).isNull();

    }

    @Test
    public void testSowStones_throwsExceptionIfGameIdArgsIsNotPassed() {

        // data
        SowStoneRequest req = new SowStoneRequest();
        req.setPlayedBy(PlayerCategory.PLAYER_A);
        req.setPitIndex(0);


        // test
        assertThrows(NotFoundException.class, ()->
                gameService.sowStones(req), MessageConstant.GAME_ID_REQUIRED);

    }

    @Test
    public void testSowStones_throwsExceptionIfPitIndexArgsIsNotPassed() {

        // data
        SowStoneRequest req = new SowStoneRequest();
        req.setPlayedBy(PlayerCategory.PLAYER_A);
        req.setGameId(String.valueOf(gameData.getId()));


        // test
        assertThrows(NotFoundException.class, ()->
                gameService.sowStones(req), MessageConstant.PIT_NO_REQUIRED);

    }

    @Test
    public void testSowStones_throwsExceptionIfGameIsCompletedAlready() {

        // data
        SowStoneRequest req = new SowStoneRequest();
        req.setPlayedBy(PlayerCategory.PLAYER_A);
        req.setGameId(String.valueOf(gameData.getId()));
        req.setPitIndex(0);

        gameData.setIsCompleted(true);
        gameData.setWinner(PlayerCategory.PLAYER_A);

        // when
        when(gameDataRepo.findById(any())).thenReturn(java.util.Optional.ofNullable(gameData));


        // test
        assertThrows(ConflictException.class, ()-> gameService.sowStones(req));
    }

    @Test
    public void testSowStones_throwsExceptionIfPlayerIsNotValid(){

        // data
        SowStoneRequest req = new SowStoneRequest();
        req.setPlayedBy(PlayerCategory.PLAYER_B);
        req.setGameId(String.valueOf(gameData.getId()));
        req.setPitIndex(0);

        // when
        when(gameDataRepo.findById(any())).thenReturn(java.util.Optional.ofNullable(gameData));

        // test
        assertThrows(ForbiddenException.class, ()-> gameService.sowStones(req),
                "PLAYER A should play next");
    }

    @Test
    public void testSowStones_throwsExceptionIfPlayerDoesNotPickStonesFromTheRightPit(){

        // data
        SowStoneRequest req = new SowStoneRequest();
        req.setPlayedBy(PlayerCategory.PLAYER_A);
        req.setGameId(String.valueOf(gameData.getId()));
        req.setPitIndex(8);

        // when
        when(gameDataRepo.findById(any())).thenReturn(java.util.Optional.ofNullable(gameData));

        // test
        assertThrows(BadRequestException.class, ()-> gameService.sowStones(req),
                MessageConstant.YOU_CAN_ONLY_PICK_STONES_FROM_YOUR_DESIGNATED_PITS);
    }

    @Test
    public void testSowStones_throwsExceptionIfStartingPitHasNoStones(){

        // data
        SowStoneRequest req = new SowStoneRequest();
        req.setPlayedBy(PlayerCategory.PLAYER_A);
        req.setGameId(String.valueOf(gameData.getId()));
        req.setPitIndex(0);

        gameData.getPits().get(0).setStoneCount(0);

        // when
        when(gameDataRepo.findById(any())).thenReturn(java.util.Optional.ofNullable(gameData));

        // test
        assertThrows(BadRequestException.class, ()-> gameService.sowStones(req),
                MessageConstant.PIT_HAS_NO_STONES);
    }

    @Test
    public void testSowStones_assertThatPlayerAGoesPlaysAgainIfHisFinalSowIsInHisMancalaPit(){

        // data
        SowStoneRequest req = new SowStoneRequest();
        req.setPlayedBy(PlayerCategory.PLAYER_A);
        req.setGameId(String.valueOf(gameData.getId()));
        req.setPitIndex(0);


        // when
        when(gameDataRepo.findById(any())).thenReturn(java.util.Optional.ofNullable(gameData));

        // test
        gameService.sowStones(req);

        // verify
        verify(gameDataRepo).save(any());
        verify(gameDataRepo).save(gameDataArgumentCaptor.capture());
        assertThat(gameDataArgumentCaptor.getValue()).isNotNull();
        assertThat(gameDataArgumentCaptor.getValue().getNextPlayer()).isEqualTo(PlayerCategory.PLAYER_A);
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(0).getStoneCount()).isEqualTo(0);
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(6).getStoneCount()).isEqualTo(1);
    }

    @Test
    public void testSowStones_scenarioOne(){

        // data
        SowStoneRequest req = new SowStoneRequest();
        req.setPlayedBy(PlayerCategory.PLAYER_A);
        req.setGameId(String.valueOf(gameData.getId()));
        req.setPitIndex(5);

        gameData.getPits().get(5).setStoneCount(9);


        // when
        when(gameDataRepo.findById(any())).thenReturn(java.util.Optional.ofNullable(gameData));

        // test
        gameService.sowStones(req);

        // verify
        verify(gameDataRepo).save(any());
        verify(gameDataRepo).save(gameDataArgumentCaptor.capture());
        assertThat(gameDataArgumentCaptor.getValue()).isNotNull();
        assertThat(gameDataArgumentCaptor.getValue().getNextPlayer()).isEqualTo(PlayerCategory.PLAYER_B);
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(6).getStoneCount()).isEqualTo(1);
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(7).getStoneCount()).isEqualTo(7);
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(8).getStoneCount()).isEqualTo(7);
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(9).getStoneCount()).isEqualTo(7);
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(10).getStoneCount()).isEqualTo(7);
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(11).getStoneCount()).isEqualTo(7);
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(12).getStoneCount()).isEqualTo(7);
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(13).getStoneCount()).isEqualTo(0);
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(0).getStoneCount()).isEqualTo(7);
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(1).getStoneCount()).isEqualTo(7);
        assertThat(gameDataArgumentCaptor.getValue().getWinner()).isNull();

    }


    @Test
    public void testSowStones_scenarioTwo(){

        // data
        SowStoneRequest req = new SowStoneRequest();
        req.setPlayedBy(PlayerCategory.PLAYER_B);
        req.setGameId(String.valueOf(gameData.getId()));
        req.setPitIndex(9);

        gameData.setNextPlayer(PlayerCategory.PLAYER_B);
        gameData.getPits().get(9).setStoneCount(4);



        // when
        when(gameDataRepo.findById(any())).thenReturn(java.util.Optional.ofNullable(gameData));

        // test
        gameService.sowStones(req);

        // verify
        verify(gameDataRepo).save(any());
        verify(gameDataRepo).save(gameDataArgumentCaptor.capture());
        assertThat(gameDataArgumentCaptor.getValue()).isNotNull();
        assertThat(gameDataArgumentCaptor.getValue().getNextPlayer()).isEqualTo(PlayerCategory.PLAYER_B);
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(9).getStoneCount()).isEqualTo(0);
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(10).getStoneCount()).isEqualTo(7);
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(11).getStoneCount()).isEqualTo(7);
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(12).getStoneCount()).isEqualTo(7);
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(13).getStoneCount()).isEqualTo(1);
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(0).getStoneCount()).isEqualTo(6);
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(1).getStoneCount()).isEqualTo(6);
        assertThat(gameDataArgumentCaptor.getValue().getWinner()).isNull();

    }

    @Test
    public void testSowStones_scenarioThree(){

        // data
        SowStoneRequest req = new SowStoneRequest();
        req.setPlayedBy(PlayerCategory.PLAYER_A);
        req.setGameId(String.valueOf(gameData.getId()));
        req.setPitIndex(0);

        gameData.getPits().get(0).setStoneCount(14);


        // when
        when(gameDataRepo.findById(any())).thenReturn(java.util.Optional.ofNullable(gameData));

        // test
        gameService.sowStones(req);

        // verify
        verify(gameDataRepo).save(any());
        verify(gameDataRepo).save(gameDataArgumentCaptor.capture());
        assertThat(gameDataArgumentCaptor.getValue()).isNotNull();
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(0).getStoneCount()).isEqualTo(1);
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(1).getStoneCount()).isEqualTo(8);
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(2).getStoneCount()).isEqualTo(7);
    }

    @Test
    public void testSowStones_scenarioFour(){

        // data
        SowStoneRequest req = new SowStoneRequest();
        req.setPlayedBy(PlayerCategory.PLAYER_A);
        req.setGameId(String.valueOf(gameData.getId()));
        req.setPitIndex(0);

        gameData.getPits().get(0).setStoneCount(20);


        // when
        when(gameDataRepo.findById(any())).thenReturn(java.util.Optional.ofNullable(gameData));

        // test
        gameService.sowStones(req);

        // verify
        verify(gameDataRepo).save(any());
        verify(gameDataRepo).save(gameDataArgumentCaptor.capture());
        assertThat(gameDataArgumentCaptor.getValue()).isNotNull();
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(0).getStoneCount()).isEqualTo(1);
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(1).getStoneCount()).isEqualTo(8);
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(2).getStoneCount()).isEqualTo(8);
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(6).getStoneCount()).isEqualTo(2);
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(7).getStoneCount()).isEqualTo(8);
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(8).getStoneCount()).isEqualTo(7);
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(13).getStoneCount()).isEqualTo(0);
    }

    @Test
    public void testSowStones_scenarioFive(){

        // data
        SowStoneRequest req = new SowStoneRequest();
        req.setPlayedBy(PlayerCategory.PLAYER_B);
        req.setGameId(String.valueOf(gameData.getId()));
        req.setPitIndex(12);

        gameData.setNextPlayer(PlayerCategory.PLAYER_B);
        gameData.getPits().get(12).setStoneCount(15);


        // when
        when(gameDataRepo.findById(any())).thenReturn(java.util.Optional.ofNullable(gameData));

        // test
        gameService.sowStones(req);

        // verify
        verify(gameDataRepo).save(any());
        verify(gameDataRepo).save(gameDataArgumentCaptor.capture());
        assertThat(gameDataArgumentCaptor.getValue()).isNotNull();
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(0).getStoneCount()).isEqualTo(8);
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(1).getStoneCount()).isEqualTo(7);
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(13).getStoneCount()).isEqualTo(2);
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(12).getStoneCount()).isEqualTo(1);
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(11).getStoneCount()).isEqualTo(7);
    }

    @Test
    public void testSowStones_scenarioFour_gameEnds(){

        // data
        SowStoneRequest req = new SowStoneRequest();
        req.setPlayedBy(PlayerCategory.PLAYER_A);
        req.setGameId(String.valueOf(gameData.getId()));
        req.setPitIndex(5);

        gameData.getPits().get(0).setStoneCount(0);
        gameData.getPits().get(1).setStoneCount(0);
        gameData.getPits().get(2).setStoneCount(0);
        gameData.getPits().get(3).setStoneCount(0);
        gameData.getPits().get(4).setStoneCount(0);
        gameData.getPits().get(5).setStoneCount(1);
        gameData.getPits().get(6).setStoneCount(40);



        // when
        when(gameDataRepo.findById(any())).thenReturn(java.util.Optional.ofNullable(gameData));

        // test
        gameService.sowStones(req);

        // verify
        verify(gameDataRepo).save(any());
        verify(gameDataRepo).save(gameDataArgumentCaptor.capture());
        assertThat(gameDataArgumentCaptor.getValue()).isNotNull();
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(0).getStoneCount()).isEqualTo(0);
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(1).getStoneCount()).isEqualTo(0);
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(2).getStoneCount()).isEqualTo(0);
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(3).getStoneCount()).isEqualTo(0);
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(4).getStoneCount()).isEqualTo(0);
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(5).getStoneCount()).isEqualTo(0);
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(6).getStoneCount()).isEqualTo(41);
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(7).getStoneCount()).isEqualTo(0);
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(8).getStoneCount()).isEqualTo(0);
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(9).getStoneCount()).isEqualTo(0);
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(10).getStoneCount()).isEqualTo(0);
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(11).getStoneCount()).isEqualTo(0);
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(12).getStoneCount()).isEqualTo(0);
        assertThat(gameDataArgumentCaptor.getValue().getWinner()).isEqualTo(PlayerCategory.PLAYER_A);
        assertThat(gameDataArgumentCaptor.getValue().getIsCompleted()).isTrue();

    }

    @Test
    public void testSowStones_scenarioFive_gameEndsWithNoWinners(){

        // data
        SowStoneRequest req = new SowStoneRequest();
        req.setPlayedBy(PlayerCategory.PLAYER_A);
        req.setGameId(String.valueOf(gameData.getId()));
        req.setPitIndex(5);

        gameData.getPits().get(0).setStoneCount(0);
        gameData.getPits().get(1).setStoneCount(0);
        gameData.getPits().get(2).setStoneCount(0);
        gameData.getPits().get(3).setStoneCount(0);
        gameData.getPits().get(4).setStoneCount(0);
        gameData.getPits().get(5).setStoneCount(1);
        gameData.getPits().get(6).setStoneCount(35);



        // when
        when(gameDataRepo.findById(any())).thenReturn(java.util.Optional.ofNullable(gameData));

        // test
        gameService.sowStones(req);

        // verify
        verify(gameDataRepo).save(any());
        verify(gameDataRepo).save(gameDataArgumentCaptor.capture());
        assertThat(gameDataArgumentCaptor.getValue()).isNotNull();
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(0).getStoneCount()).isEqualTo(0);
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(1).getStoneCount()).isEqualTo(0);
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(2).getStoneCount()).isEqualTo(0);
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(3).getStoneCount()).isEqualTo(0);
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(4).getStoneCount()).isEqualTo(0);
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(5).getStoneCount()).isEqualTo(0);
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(6).getStoneCount()).isEqualTo(36);
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(7).getStoneCount()).isEqualTo(0);
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(8).getStoneCount()).isEqualTo(0);
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(9).getStoneCount()).isEqualTo(0);
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(10).getStoneCount()).isEqualTo(0);
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(11).getStoneCount()).isEqualTo(0);
        assertThat(gameDataArgumentCaptor.getValue().getPits().get(12).getStoneCount()).isEqualTo(0);
        assertThat(gameDataArgumentCaptor.getValue().getWinner()).isNull();
        assertThat(gameDataArgumentCaptor.getValue().getIsCompleted()).isTrue();

    }

    @Test
    public void testBuildPits(){

        // when
        when(gameProperties.getNumberOfPits()).thenReturn(14);
        when(gameProperties.getNumberOfStonesPerPit()).thenReturn(6);

        // test
        List<Pit> pits = gameService.buildPits();

        // verify
        assertThat(pits).isNotEmpty();
        assertThat(pits.size()).isEqualTo(14);
        assertThat(pits.get(0).getStoneCount()).isEqualTo(6);
        assertThat(pits.get(1).getStoneCount()).isEqualTo(6);
        assertThat(pits.get(2).getStoneCount()).isEqualTo(6);
        assertThat(pits.get(3).getStoneCount()).isEqualTo(6);
        assertThat(pits.get(4).getStoneCount()).isEqualTo(6);
        assertThat(pits.get(5).getStoneCount()).isEqualTo(6);
        assertThat(pits.get(6).getStoneCount()).isEqualTo(0);
        assertThat(pits.get(7).getStoneCount()).isEqualTo(6);
        assertThat(pits.get(8).getStoneCount()).isEqualTo(6);
        assertThat(pits.get(9).getStoneCount()).isEqualTo(6);
        assertThat(pits.get(10).getStoneCount()).isEqualTo(6);
        assertThat(pits.get(11).getStoneCount()).isEqualTo(6);
        assertThat(pits.get(12).getStoneCount()).isEqualTo(6);
        assertThat(pits.get(13).getStoneCount()).isEqualTo(0);


    }

    @Test
    public void testUpdatePitsIfTheLastPitSowedIsAnOwnEmptyPit(){

        // data
        List<Pit> pits = TestUtils.generatePits();
        pits.get(5).setStoneCount(1);

        int indexOfLastSowedPit = 5;
        PlayerCategory player = PlayerCategory.PLAYER_A;

        // test
        List<Pit> newPits = gameService.updatePitsIfTheLastPitSowedIsAnOwnEmptyPit(player, pits,
                indexOfLastSowedPit);

        // verify
        assertThat(newPits).isNotEmpty();
        assertThat(pits.size()).isEqualTo(14);
        assertThat(pits.get(0).getStoneCount()).isEqualTo(6);
        assertThat(pits.get(1).getStoneCount()).isEqualTo(6);
        assertThat(pits.get(2).getStoneCount()).isEqualTo(6);
        assertThat(pits.get(3).getStoneCount()).isEqualTo(6);
        assertThat(pits.get(4).getStoneCount()).isEqualTo(6);
        assertThat(pits.get(5).getStoneCount()).isEqualTo(0);
        assertThat(pits.get(6).getStoneCount()).isEqualTo(7);
        assertThat(pits.get(7).getStoneCount()).isEqualTo(0);
        assertThat(pits.get(8).getStoneCount()).isEqualTo(6);
        assertThat(pits.get(9).getStoneCount()).isEqualTo(6);
        assertThat(pits.get(10).getStoneCount()).isEqualTo(6);
        assertThat(pits.get(11).getStoneCount()).isEqualTo(6);
        assertThat(pits.get(12).getStoneCount()).isEqualTo(6);
        assertThat(pits.get(13).getStoneCount()).isEqualTo(0);
    }

    @Test
    public void testHasGameEnded(){

        // data
        List<Pit> pits = TestUtils.generatePits();
        pits.get(0).setStoneCount(0);
        pits.get(1).setStoneCount(0);
        pits.get(2).setStoneCount(0);
        pits.get(3).setStoneCount(0);
        pits.get(4).setStoneCount(0);
        pits.get(5).setStoneCount(0);

        // test and verify
        assertThat(gameService.hasTheGameEnded(pits)).isTrue();
    }

    @Test
    public void testGetOppositePitIndex() {

        // test and verify
        assertThat(gameService.getOppositePitIndex(0)).isEqualTo(12);
        assertThat(gameService.getOppositePitIndex(2)).isEqualTo(10);
        assertThat(gameService.getOppositePitIndex(5)).isEqualTo(7);
        assertThat(gameService.getOppositePitIndex(7)).isEqualTo(5);
        assertThat(gameService.getOppositePitIndex(12)).isEqualTo(0);
    }

    @Test
    public void testGetGameWinner() {

        // data
        List<Pit> pits = TestUtils.generatePits();
        pits.get(6).setStoneCount(13);
        pits.get(13).setStoneCount(10);


        // test and verify
        assertThat(gameService.getGameWinner(pits)).isEqualTo(PlayerCategory.PLAYER_A);
    }

    @Test
    public void testGetGameWinner_returnsNoWinnerIfBothMancalaPitsAreEqual() {

        // data
        List<Pit> pits = TestUtils.generatePits();
        pits.get(6).setStoneCount(13);
        pits.get(13).setStoneCount(13);


        // test and verify
        assertThat(gameService.getGameWinner(pits)).isNull();
    }
}
